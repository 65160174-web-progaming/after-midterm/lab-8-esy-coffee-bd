import { AppDataSource } from "./data-source"
import { Role } from "./entity/Role"
import { User } from "./entity/User"

AppDataSource.initialize().then(async () => {
    const rolesRespository = AppDataSource.getRepository(Role)
    const adminRole = await rolesRespository.findOneBy({ id: 1 })
    const userRole = await rolesRespository.findOneBy({ id: 2 })


    const usersRepository = AppDataSource.getRepository(User)
    await usersRepository.clear();

    console.log("Inserting a new user into the Memory...")
    var user = new User()
    user.id = 1
    user.email = "Admin@email.com"
    user.password = "Pass@1234"
    user.gender = "male"
    user.roles = [adminRole, userRole]
    console.log("Inserting a new user into the SQLITE...")
    await usersRepository.save(user)

    const admin = await  usersRepository.findOneBy({ id: 1 })
    console.log("Loaded Admin:", admin)

    user = new User()
    user.id = 2
    user.email = "User1@email.com"
    user.password = "Pass@1234"
    user.gender = "male"
    user.roles = [userRole]
    await usersRepository.save(user)

    const user1 = await  usersRepository.findOneBy({ id: 2 })
    console.log("Loaded User1:", user1)

    user = new User()
    user.id = 3
    user.email = "User2@email.com"
    user.password = "Pass@1234"
    user.gender = "female"
    user.roles = [userRole]
    await usersRepository.save(user)

console.log("All User:", JSON.stringify(await usersRepository.find({ relations: ['roles'] }), null, 2));
console.log("All Role:", JSON.stringify(await rolesRespository.find({ relations: ['users'] }), null, 2));
    

}).catch(error => console.log(error))
