import { AppDataSource } from "./data-source"
import { Role } from "./entity/Role";
import { User } from "./entity/User";

AppDataSource.initialize().then(async () => {
    const rolesRepository = AppDataSource.getRepository(Role)
    await rolesRepository.clear();

    var role = new Role()
    role.id = 1
    role.name = "admin"
    console.log("Inserting a new role into the SQLITE...")
    await rolesRepository.save(role)


    role = new Role()
    role.id = 2
    role.name = "user"
    console.log("Inserting a new role into the SQLITE...")
    await rolesRepository.save(role)

    console.log("Find All : ", await rolesRepository.find({ order: { id: "ASC" } }))
    
    


}).catch(error => console.log(error))
