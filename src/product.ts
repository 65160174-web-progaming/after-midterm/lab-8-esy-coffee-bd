import { AppDataSource } from "./data-source"
import { Product } from "./entity/Product";
import { Type } from './entity/Type';

AppDataSource.initialize().then(async () => {
    const typesResipotory = AppDataSource.getRepository(Type);
    const drinkType = await typesResipotory.findOneBy({ id: 1 })
    const bekeryType = await typesResipotory.findOneBy({ id: 2 })
    const foodType = await typesResipotory.findOneBy({ id: 3 })

    const productsResipotory = AppDataSource.getRepository(Product);
    await productsResipotory.clear();

    var product = new Product();
    product.id = 1
    product.name = "Americano"
    product.price = 50
    product.type = drinkType
    await productsResipotory.save(product)

    product = new Product();
    product.id = 2
    product.name = "Chocolate Cake" 
    product.price = 45
    product.type = bekeryType
    await productsResipotory.save(product)

    product = new Product();
    product.id = 3
    product.name = "Lab"
    product.price = 80
    product.type = foodType
    await productsResipotory.save(product)

    product = new Product();
    product.id = 4
    product.name = "Tom Zab"
    product.price = 80
    product.type = foodType
    await productsResipotory.save(product)

    product = new Product();
    product.id = 5
    product.name = "Magarong" 
    product.price = 75
    product.type = bekeryType
    await productsResipotory.save(product)

    product = new Product();
    product.id = 6
    product.name = "Dang Soda" 
    product.price = 40
    product.type = drinkType
    await productsResipotory.save(product)

    console.log("All Product :" ,await productsResipotory.find( {order: {id: "ASC"} ,relations: ['type']} ))

}).catch (error => console.log(error))
