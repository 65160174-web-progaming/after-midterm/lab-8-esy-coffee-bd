import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, ManyToOne, OneToOne } from "typeorm"
import { Type } from "./Type";
import { Order } from "./Order";
import { Product } from './Product';

@Entity()
export class OrderItem {

    @PrimaryGeneratedColumn()
    id: number

    @Column()
    name: string

    @Column({
        type: "decimal",
        precision: 10,
        scale: 2,
    })
    price: number

    @Column()
    qty:number

    @Column({
        type: "decimal",
        precision: 10,
        scale: 2,
    })
    total:number

    @ManyToOne(() => Order, (order) => order.orderItems)
    order: Order
    
    @ManyToOne(() => Product, (product) => product.orderItems)
    product: Product
    
   
    @CreateDateColumn()
    created: Date;
  
    @UpdateDateColumn()
    updated: Date;

   


    
}
