import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, OneToMany, ManyToMany, JoinTable } from "typeorm"
import { Role } from "./Role"
import { Order } from "./Order"

@Entity()
export class User {

    @PrimaryGeneratedColumn()
    id: number

    @Column()
    email: string

    @Column()
    password: string

    @Column()
    gender: string

    @CreateDateColumn()
    created: Date;
  
    @UpdateDateColumn()
    updated: Date;

    @ManyToMany(() => Role, role => role.users,{cascade: true})
    @JoinTable()
    roles: Role[]
   
    @OneToMany(() => Order, (order) => order.user)
    orders:Order[]
    
    
    
    

}
