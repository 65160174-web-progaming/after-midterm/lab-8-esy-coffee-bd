import { AppDataSource } from "./data-source";
import { Type } from "./entity/Type";

AppDataSource.initialize().then(async () => {
    const typesRepository = AppDataSource.getRepository(Type);
    await typesRepository.clear();

    var type = new Type();
    type.id = 1
    type.name = "Drinks"
    await typesRepository.save(type)

    type = new Type();
    type.id = 2
    type.name = "Bekeries"
    await typesRepository.save(type)

    type = new Type();
    type.id = 3
    type.name = "Foods"
    await typesRepository.save(type)

    console.log("All Types :", await typesRepository.find({ order: { id: "ASC" } }))
    
}).catch(error => console.log(error));
