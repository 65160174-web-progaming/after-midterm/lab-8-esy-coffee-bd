import { AppDataSource } from "./data-source"
import { Order } from "./entity/Order"
import { OrderItem } from './entity/OrderItem';
import { Product } from "./entity/Product"
import { User } from "./entity/User"

const orderDto ={
    orderItems:[
        {productId: 1, qty: 2},
        {productId: 2, qty: 3},
    ],
    userId:2,
    total:0

}

AppDataSource.initialize().then(async () => {
    const usersRepository = AppDataSource.getRepository(User)
    const productRepository = AppDataSource.getRepository(Product)
    const orderItemRepository = AppDataSource.getRepository(OrderItem)
    const orderRespository = AppDataSource.getRepository(Order)

    const user = await usersRepository.findOneBy({ id: orderDto.userId })

    var order = new Order();
    order.user = user;
    order.total = 0;
    order.qty = 0;
    order.orderItems = []
    for(const oi of orderDto.orderItems){
        const orderItem = new OrderItem();
        orderItem.product = await productRepository.findOneBy({ id: oi.productId });
        orderItem.name = orderItem.product.name;
        orderItem.price = orderItem.product.price;
        orderItem.qty = oi.qty;
        orderItem.total =  orderItem.price * orderItem.qty
        await orderItemRepository.save(orderItem)
        order.orderItems.push(orderItem)
        order.total += orderItem.total
        order.qty += orderItem.qty
    }

    
    
    await orderRespository.save(order)

    const allOrder = await orderRespository.find( {relations: ['user','orderItems']} )

    console.log ("All Order :" ,JSON.stringify(allOrder,null,2))

    

}).catch(error => console.log(error))
